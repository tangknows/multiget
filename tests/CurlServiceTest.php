<?php

use PHPUnit\Framework\TestCase;
require_once('CurlService.php');


class CurlServiceTest extends TestCase {

    const URL = "http://www.example.com/";
    const FILE_LOCATION = "assets/example.html";
    const HTTP_OK_RESPONSE = 200;
    const HTTP_PARTIAL_RESPONSE = 206;

    public function testExecuteCurl() {
        $this->cleanUp();

        $curlService = new CurlService();
        $curlService->setFileName("example.html");
        $curlService->setUrl(self::URL);
        $results = $curlService->executeCurl();

        $this->assertEquals($curlService->getCurlStatus(), SELF::HTTP_OK_RESPONSE);
        $this->assertStringNotEqualsFile(self::FILE_LOCATION, '');
    }

    public function testExecuteCurlWithinRanges0To15() {
        $this->cleanUp();

        $lower_range = 0;
        $upper_range = 14;
        $curlService = new CurlService();
        $curlService->setFileName("example.html");
        $curlService->setUrl(self::URL);
        $curlService->setRange($lower_range, $upper_range);
        $results = $curlService->executeCurl();

        $this->assertEquals($curlService->getCurlStatus(), self::HTTP_PARTIAL_RESPONSE);
        $this->assertEquals(15, filesize(self::FILE_LOCATION));
    }

    public function testExecuteCurlWithinRanges16To30() {
        $this->cleanUp();

        $lower_range = 15;
        $upper_range = 29;
        $curlService = new CurlService();
        $curlService->setFileName("example.html");
        $curlService->setUrl(self::URL);
        $curlService->setRange($lower_range, $upper_range);
        $results = $curlService->executeCurl();
        
        $this->assertEquals($curlService->getCurlStatus(), self::HTTP_PARTIAL_RESPONSE);
        $this->assertEquals(15, filesize(self::FILE_LOCATION));
    }

    public function cleanUp() {
        if (file_exists(self::FILE_LOCATION)){
            unlink(self::FILE_LOCATION);
        }
    }

}