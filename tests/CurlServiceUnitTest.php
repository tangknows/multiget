<?php

use PHPUnit\Framework\TestCase;
require_once('CurlService.php');


class CurlServiceUnitTest extends TestCase {

    const URL = "http://www.example.com/";
    const LOWER_RANGE = "0";
    const UPPER_RANGE = "100";

    public function testCanGetSetUrl() {
        $curlService = new CurlService();
        $curlService->setUrl(self::URL);
        $this->assertEquals($curlService->getUrl(), self::URL);
    }

    public function testCanSetGetRange() {
        $curlService = new CurlService();
        $curlService->setRange(self::LOWER_RANGE, self::UPPER_RANGE);
        $this->assertEquals($curlService->getLowerRange(), self::LOWER_RANGE);
        $this->assertEquals($curlService->getUpperRange(), self::UPPER_RANGE);
    }

    public function testCanGetRangeAsString() {
        $range_as_string = self::LOWER_RANGE . "-" . self::UPPER_RANGE;
        $curlService = new CurlService();
        $curlService->setRange(self::LOWER_RANGE, self::UPPER_RANGE);
        $this->assertEquals($curlService->getRangeAsString(), $range_as_string);
    }

    public function testGetCurlResults() {
        $data = "some data here";
        $curlService = new CurlService();
        $curlService->setCurlResults($data);
        $this->assertEquals($curlService->getCurlResults(), $data);
    }

    public function testGetCurlStatus() {
        $data = "some data here";
        $curlService = new CurlService();
        $curlService->setCurlStatus($data);
        $this->assertEquals($curlService->getCurlStatus(), $data);
    }

    public function getMockCurlService() {
        $mockCurlService = $this->getMockBuilder(CurlService::class)
                                ->disableOriginalConstructor()->getMock();
        return $mockCurlService;
    }

    public function testExecuteCurl() {
        $data = "curl requested data";
        $mockCurlService = $this->getMockCurlService();
        $mockCurlService->expects($this->once())
                        ->method('executeCurl')
                        ->will($this->returnValue($data));
        $results = $mockCurlService->executeCurl();
        $this->assertEquals($results, $data);

    }


}