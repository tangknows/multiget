<?php

use PHPUnit\Framework\TestCase;
require_once('DownloadManager.php');


class DownloadManagerTest extends TestCase {

    const DIRECTORY = "assets/";
    const URL = "http://www.example.com/";
    const FILE_LOCATION = "assets/example.html";

    public function testCanDownloadFilesInChunks() {
        $this->cleanUp();
        $downloadManager = new DownloadManager();
        $chunk_size_in_bytes = 200;
        $file_name = "example.html";
        $downloadManager->download_files_in_chunks(self::URL, $chunk_size_in_bytes, $file_name);
        $this->assertEquals(1270, filesize(self::DIRECTORY . $file_name));
    }

    public function cleanUp() {
        if (file_exists(self::FILE_LOCATION)){
            unlink(self::FILE_LOCATION);
        }
    }
}