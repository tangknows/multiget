<?php

use PHPUnit\Framework\TestCase;
require_once('DownloadManager.php');


class DownloadManagerUnitTest extends TestCase {

    public function getMockDownloadManager() {
        return $this->getMockBuilder(DownloadManager::class)->disableOriginalConstructor()->getMock();
    }

    public function testCanBeCreatedWithParameters() {
        $url = "http://www.example.com/";
        $chunk_size_in_bytes = 15;
        $file_location = "/assets/example.html";

        $mock_download_manager = $this->getMockDownloadManager();

        $mock_download_manager->expects($this->once())
                        ->method('download_files_in_chunks')
                        ->with($url, $chunk_size_in_bytes, $file_location)
                        ->will($this->returnValue("download status"));

        $results = $mock_download_manager->download_files_in_chunks($url, $chunk_size_in_bytes, $file_location);

        $this->assertEquals($results, "download status");
    }


}