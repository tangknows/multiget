<?php

require_once('CurlService.php');

class DownloadManager {

	private $curl_service;

    public function __construct() {
        $this->curl_service = new CurlService();
    }

    public function download_files_in_chunks(string $url, int $chunk_size_in_bytes, string $file_name) {
		$lower_range = 0;
		$upper_range = $chunk_size_in_bytes;
		$http_success_codes = [200, 206];
		$file_location = CurlService::DIRECTORY . $file_name;

    	while (true){
	    	$this->curl_service->setUrl($url);
	    	$this->curl_service->setFileName($file_name);
	    	$this->curl_service->setRange($lower_range, $upper_range);
	    	$this->curl_service->executeCurl();

	    	$curl_service_success = in_array($this->curl_service->getCurlStatus(), $http_success_codes);

	    	if (!$curl_service_success){
	    		break;
	    	}

	    	if (!file_exists($file_location)){
	    		break;
	    	}

    		$file_size = filesize($file_location);

    		$download_complete = $file_size < $upper_range || $file_size > CurlService::SIZE_LIMIT; 
    		if ($download_complete){
    			break;
    		}

			$lower_range = $file_size;
			$upper_range = $lower_range + $chunk_size_in_bytes;

			clearstatcache();
    	}
    	
    	return $this->curl_service->getCurlStatus();
    }


    

}