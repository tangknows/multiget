<?php

class CurlService {

    const DIRECTORY = "assets/";
    const SIZE_LIMIT = 4194304; // 4 MiB in bytes

    private $file_name;
    private $url;
    private $lower_range;
    private $upper_range;
    private $curl_results;
    private $curl_status;

    public function setFileName($file_name) {
        $this->file_name = $file_name;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setRange(int $lower_range, int $upper_range) {
        $this->lower_range = $lower_range;
        $this->upper_range = $upper_range;
    }

    public function setCurlResults($curl_results) {
        $this->curl_results = $curl_results;
    }

    public function setCurlStatus($curl_status) {
        $this->curl_status = $curl_status;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getLowerRange() {
        return $this->lower_range;
    }
    
    public function getUpperRange() {
        return $this->upper_range;
    }    

    public function getCurlResults() {
        return $this->curl_results;
    }    
    
    public function getCurlStatus() {
        return $this->curl_status;
    }

    public function getRangeAsString() {
        return $this->lower_range . "-" . $this->upper_range;
    }

    public function isRangeIncluded() {
        return is_int($this->lower_range) && is_int($this->upper_range);
    }

    private function getFile() {
        $file_location = self::DIRECTORY . $this->file_name;
        return fopen($file_location, "a");
    }

    public function executeCurl() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FILE, $this->getFile());
        if ($this->isRangeIncluded()){
            curl_setopt($ch, CURLOPT_RANGE, $this->getRangeAsString() );
        }
        $this->curl_results = curl_exec($ch);
        $this->curl_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if(curl_errno($ch)){
            throw new Exception(curl_error($ch));
        }

        curl_close($ch);
    }



}