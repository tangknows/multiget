<?php

require_once('DownloadManager.php');

echo "Starting Multiget...\n";
echo "\n";

$url = readline("Please enter url link to download from: ");
$chunk_size_in_bytes = readline("Please enter chunk size in bytes format (1048576 in 1 MiB): ");
$file_name = readline("Please add a file name, please include file format (.png, .jar, .html) : ");

$url = (!trim($url)) ? "http://a1fc88e1.bwtest-aws.pravala.com/384MB.jar" : $url;
$chunk_size_in_bytes = (!trim($chunk_size_in_bytes)) ? 1048576 : $chunk_size_in_bytes;
$file_name = (!trim($file_name)) ? "default.jar" : $file_name;

$file_location = "assets/" . $file_name;

remove_file_if_exists($file_location);
get_starting_download_text($url, $chunk_size_in_bytes, $file_name);

$downloadManager = new DownloadManager();
$results = $downloadManager->download_files_in_chunks($url, $chunk_size_in_bytes, $file_name);

echo "Download complete\n";

?>

<?php
function remove_file_if_exists($file_location) {
	if (file_exists($file_location)){
		$remove_file = readline("File already exists, do you want to continue? Original will be removed and new file will save in its place?: (y/n) ");
		if (trim(strtolower($remove_file)) == "y"){
			unlink($file_location);
		}
		else {
			echo "exiting...\n";
			exit;
		}
	}
}

function get_starting_download_text($url, $chunk_size_in_bytes, $file_name){
	echo "\n";
	echo 'Thank you you chose the following options: '. $url . "\n";
	echo 'url: '. $url . "\n";
	echo 'chunk size: '. $chunk_size_in_bytes . "\n";
	echo 'file name: '. $file_name . "\n\n";
	echo "Download starting, please note for testing purposes this will only get 4Mib worth of data. Thank you.\n";
}
?>