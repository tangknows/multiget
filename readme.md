## To Run Multiget

From project directory run commandg below:  
php MultiGet.php      
If no options are selected, defaults are provided;    

## To Run Phpunit tests

From project directory run commands below:    
php bin/phpunit-7.3.5.phar tests/CurlServiceTest.php.  
php bin/phpunit-7.3.5.phar tests/CurlServiceUnitTest.php     
php bin/phpunit-7.3.5.phar tests/DownloadManagerTest.php   
php bin/phpunit-7.3.5.phar tests/DownloadManagerUnitTest.php   
